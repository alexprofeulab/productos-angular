import { Observable, throwError } from 'rxjs';
import { map, catchError, retry } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { IProduct } from '../interfaces/i-product';
import { HttpClient } from '@angular/common/http';
import { ResponseProduct } from '../interfaces/response-product';
import { ResponseProducts } from '../interfaces/response-products';
import { HttpErrorResponse } from '../interfaces/http-error-response';
import { OkResponse } from '../interfaces/ok-response';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private productUrl = 'http://localhost/productos';

  getProducts() : Observable<IProduct []> {
    return this.http.get<ResponseProducts>(this.productUrl).pipe(
      map(response => response.data), 
      catchError((resp: HttpErrorResponse) => throwError('Error obteniendo productos. Código de servidor: ${resp.status}. Mensaje: ${resp.message}'))
    );
  }

  getProduct(id: number) : Observable<IProduct> {
    return this.http.get<ResponseProduct>(this.productUrl + '/' + id).pipe(
      map(response => response.data), 
      catchError((resp: HttpErrorResponse) => throwError('Error obteniendo productos. Código de servidor: ${resp.status}. Mensaje: ${resp.message}'))
    );
  }

  changeRating(idProduct: number, rating: number) : Observable<boolean> {
    return this.http.put<OkResponse>(this.productUrl + '/rating/' + idProduct, {rating: rating}).pipe(
      map(response => {
        if (!response.ok) throw response.mensaje;
        return true;
      }), 
      catchError((resp: HttpErrorResponse) => throwError('Error cambiando el rating del producto ' + idProduct + '. Código de servidor: ${resp.status}. Mensaje: ${resp.mensaje}'))
    );
  }

  constructor(private http: HttpClient) { }
}
