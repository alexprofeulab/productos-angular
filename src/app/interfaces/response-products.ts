import { IProduct } from './i-product';

export interface ResponseProducts {
    data: IProduct[];
}
