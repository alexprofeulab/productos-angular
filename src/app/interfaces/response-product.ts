import { IProduct } from './i-product';

export interface ResponseProduct {
    data: IProduct
}
