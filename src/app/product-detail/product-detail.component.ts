import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ProductsService } from '../services/products.service';
import {IProduct } from '../interfaces/i-product';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  product: IProduct;

  constructor(private route: ActivatedRoute,
              private productsService: ProductsService, 
              private router: Router) { }

  goBack() {
    this.router.navigate(['/products'])
  }

  changeRating(rating: number) {
    this.productsService.changeRating(this.product.id, rating).subscribe(
      ok => this.product.rating = rating,
      error => console.error(error), 
      () => console.log("Rating changed")
    );
  }

  ngOnInit(): void {
    const id = +this.route.snapshot.params['id'];

    this.productsService.getProduct(id).subscribe(
      p => this.product = p,
      error => console.log(error)
    )
  }
}
